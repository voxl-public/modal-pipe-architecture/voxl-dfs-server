/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// Project Includes
#include "sad_disparity_estimation.h"

#include <iostream>

// The Kernel variables we will be using to load the opencl disparity kernel code
extern "C" const char disparity_kernel_code[];
extern "C" const size_t disparity_kernel_code_len;

extern "C" const char compute_dsi_kernel_code[];
extern "C" const size_t compute_dsi_kernel_code_len;

extern "C" const char sparse_census_transform_kernel_code[];
extern "C" const size_t sparse_census_transform_kernel_code_len;



SADDisparityEstimator::SADDisparityEstimator(OpenCLManager& opencl_manager_in, int image_width_in, int image_height_in, int max_disparity_in):
    opencl_manager(opencl_manager_in),
    disparity_kernel(opencl_manager_in, "censusDisparity"),
    census_kernel_l(opencl_manager_in, "sparseCensusTransform"),
    census_kernel_r(opencl_manager_in, "sparseCensusTransform"),
    compute_dsi_left_kernel(opencl_manager_in, "computeDSILeft"),
    compute_dsi_right_kernel(opencl_manager_in, "computeDSIRight"),
    compute_left_disparity_from_dsi_kernel(opencl_manager_in, "computeDisparityFromDSI"),
    compute_right_disparity_from_dsi_kernel(opencl_manager_in, "computeDisparityFromDSI"),
    left_right_consistency_check_kernel(opencl_manager_in, "leftRightConsistencyCheck"),
    image_width(image_width_in),
    image_height(image_height_in),
    max_disparity(max_disparity_in)
{
    // Setup the buffers
    this->in_left_img_buffer_image2d = cl::Image2D (this->opencl_manager.getContext(), CL_MEM_READ_ONLY, cl::ImageFormat(CL_R, CL_UNSIGNED_INT8), this->image_width, this->image_height, 0, nullptr, nullptr);
    this->in_right_img_buffer_image2d = cl::Image2D (this->opencl_manager.getContext(), CL_MEM_READ_ONLY, cl::ImageFormat(CL_R, CL_UNSIGNED_INT8), this->image_width, this->image_height, 0, nullptr, nullptr);
    this->out_img_buffer_image2d = cl::Image2D (this->opencl_manager.getContext(), CL_MEM_READ_WRITE, cl::ImageFormat(CL_R, CL_UNSIGNED_INT8), this->image_width, this->image_height, 0, nullptr, nullptr);
    this->census_left_img_buffer_image2d = cl::Image2D (this->opencl_manager.getContext(), CL_MEM_READ_WRITE, cl::ImageFormat(CL_RG, CL_UNSIGNED_INT32), this->image_width, this->image_height, 0, nullptr, nullptr);
    this->census_right_img_buffer_image2d = cl::Image2D (this->opencl_manager.getContext(), CL_MEM_READ_WRITE, cl::ImageFormat(CL_RG, CL_UNSIGNED_INT32), this->image_width, this->image_height, 0, nullptr, nullptr);
    this->left_disparaty_img_buffer_image2d = cl::Image2D (this->opencl_manager.getContext(), CL_MEM_READ_WRITE, cl::ImageFormat(CL_R, CL_UNSIGNED_INT8), this->image_width, this->image_height, 0, nullptr, nullptr);
    this->right_disparaty_img_buffer_image2d = cl::Image2D (this->opencl_manager.getContext(), CL_MEM_READ_WRITE, cl::ImageFormat(CL_R, CL_UNSIGNED_INT8), this->image_width, this->image_height, 0, nullptr, nullptr);
    this->dsi_left_buffer = cl::Buffer(this->opencl_manager.getContext(), CL_MEM_READ_WRITE, image_width * image_height * max_disparity);
    this->dsi_right_buffer = cl::Buffer(this->opencl_manager.getContext(), CL_MEM_READ_WRITE, image_width * image_height * max_disparity);

    // Init the kernel
    this->initDisparityKernel();
    this->initCensusKernel();
    this->initComputeDSIKernel();
    this->initComputeDisparityFromDSIKernel();
    this->initLeftRightConsistencyCheckKernel();
}

SADDisparityEstimator::~SADDisparityEstimator()
{

}

void SADDisparityEstimator::initDisparityKernel()
{
    try
    {
        // Create the local and global sizes and set them
        int lx = LOCAL_WORK_GROUP_X;
        int ly = LOCAL_WORK_GROUP_Y;
        int gx = OpenCLKernel::roundUp(this->image_width, lx);
        int gy = OpenCLKernel::roundUp(this->image_height, ly);
        this->disparity_kernel.setWorkGroupsAndOffset(cl::NDRange(lx, ly), cl::NDRange(gx, gy), cl::NDRange(0, 0));

        // Create the compile options for this kernel
        std::stringstream options("");
        options << " -DWIDTH="         << this->image_width;
        options << " -DHEIGHT="        << this->image_height;
        options << " -DMAX_DISPARITY=" << this->max_disparity;
        options << " -DINVALID_DISPARITY_VALUE=" << INVALID_DISPARITY_VALUE;

        // Compile the kernel!
        std::string kernel_code(disparity_kernel_code);
        this->disparity_kernel.compileKernel(kernel_code, options.str());
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}


void SADDisparityEstimator::initCensusKernel()
{
    try
    {
        // Create the local and global sizes and set them
        int lx = LOCAL_WORK_GROUP_X;
        int ly = LOCAL_WORK_GROUP_Y;
        int gx = OpenCLKernel::roundUp(this->image_width, lx);
        int gy = OpenCLKernel::roundUp(this->image_height, ly);
        this->census_kernel_l.setWorkGroupsAndOffset(cl::NDRange(lx, ly), cl::NDRange(gx, gy), cl::NDRange(0, 0));
        this->census_kernel_r.setWorkGroupsAndOffset(cl::NDRange(lx, ly), cl::NDRange(gx, gy), cl::NDRange(0, 0));

        // Create the compile options for this kernel
        std::stringstream options("");
        options << " -DWIDTH="         << this->image_width;
        options << " -DHEIGHT="        << this->image_height;
        options << " -DMAX_DISPARITY=" << this->max_disparity;

        // Compile the kernel!
        std::string kernel_code(sparse_census_transform_kernel_code);
        this->census_kernel_l.compileKernel(kernel_code, options.str());
        this->census_kernel_r.compileKernel(kernel_code, options.str());
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}


void SADDisparityEstimator::initComputeDSIKernel()
{
    try
    {
        // Create the local and global sizes and set them
        int lx = LOCAL_WORK_GROUP_X;
        int ly = LOCAL_WORK_GROUP_Y;
        int gx = OpenCLKernel::roundUp(this->image_width, lx);
        int gy = OpenCLKernel::roundUp(this->image_height, ly);
        this->compute_dsi_left_kernel.setWorkGroupsAndOffset(cl::NDRange(lx, ly), cl::NDRange(gx, gy), cl::NDRange(0, 0));
        this->compute_dsi_right_kernel.setWorkGroupsAndOffset(cl::NDRange(lx, ly), cl::NDRange(gx, gy), cl::NDRange(0, 0));

        // Create the compile options for this kernel
        std::stringstream options("");
        options << " -DWIDTH="         << this->image_width;
        options << " -DHEIGHT="        << this->image_height;
        options << " -DMAX_DISPARITY=" << this->max_disparity;

        // Compile the kernel!
        std::string kernel_code(compute_dsi_kernel_code);
        this->compute_dsi_left_kernel.compileKernel(kernel_code, options.str());
        this->compute_dsi_right_kernel.compileKernel(kernel_code, options.str());
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}

void SADDisparityEstimator::initComputeDisparityFromDSIKernel()
{
    try
    {
        // Create the local and global sizes and set them
        int lx = LOCAL_WORK_GROUP_X;
        int ly = LOCAL_WORK_GROUP_Y;
        int gx = OpenCLKernel::roundUp(this->image_width, lx);
        int gy = OpenCLKernel::roundUp(this->image_height, ly);
        this->compute_left_disparity_from_dsi_kernel.setWorkGroupsAndOffset(cl::NDRange(lx, ly), cl::NDRange(gx, gy), cl::NDRange(0, 0));
        this->compute_right_disparity_from_dsi_kernel.setWorkGroupsAndOffset(cl::NDRange(lx, ly), cl::NDRange(gx, gy), cl::NDRange(0, 0));


        // Create the compile options for this kernel
        std::stringstream options("");
        options << " -DWIDTH="         << this->image_width;
        options << " -DHEIGHT="        << this->image_height;
        options << " -DMAX_DISPARITY=" << this->max_disparity;
        options << " -DINVALID_DISPARITY_VALUE=" << INVALID_DISPARITY_VALUE;

        // Compile the kernel!
        std::string kernel_code(disparity_kernel_code);
        this->compute_left_disparity_from_dsi_kernel.compileKernel(kernel_code, options.str());
        this->compute_right_disparity_from_dsi_kernel.compileKernel(kernel_code, options.str());
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}


void SADDisparityEstimator::initLeftRightConsistencyCheckKernel()
{
    try
    {
        // Create the local and global sizes and set them
        int lx = LOCAL_WORK_GROUP_X;
        int ly = LOCAL_WORK_GROUP_Y;
        int gx = OpenCLKernel::roundUp(this->image_width, lx);
        int gy = OpenCLKernel::roundUp(this->image_height, ly);
        this->left_right_consistency_check_kernel.setWorkGroupsAndOffset(cl::NDRange(lx, ly), cl::NDRange(gx, gy), cl::NDRange(0, 0));


        // Create the compile options for this kernel
        std::stringstream options("");
        options << " -DWIDTH="         << this->image_width;
        options << " -DHEIGHT="        << this->image_height;
        options << " -DMAX_DISPARITY=" << this->max_disparity;
        options << " -DINVALID_DISPARITY_VALUE=" << INVALID_DISPARITY_VALUE;

        // Compile the kernel!
        std::string kernel_code(disparity_kernel_code);
        this->left_right_consistency_check_kernel.compileKernel(kernel_code, options.str());
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}

void SADDisparityEstimator::compute(cl::Image2D &left_image, cl::Image2D& right_image)
{
    try
    {
        // Fill in the kernel arguments
        int i = 0;
        this->census_kernel_l.getKernel().setArg(i++, sizeof(cl_mem), &(left_image));
        this->census_kernel_l.getKernel().setArg(i++, sizeof(cl_mem), &(this->census_left_img_buffer_image2d));
        this->census_kernel_l.launchKernel();

        i = 0;
        this->census_kernel_r.getKernel().setArg(i++, sizeof(cl_mem), &(right_image));
        this->census_kernel_r.getKernel().setArg(i++, sizeof(cl_mem), &(this->census_right_img_buffer_image2d));
        this->census_kernel_r.launchKernel();

        i = 0;
        this->compute_dsi_left_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(census_left_img_buffer_image2d));
        this->compute_dsi_left_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(census_right_img_buffer_image2d));
        this->compute_dsi_left_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(this->dsi_left_buffer));
        this->compute_dsi_left_kernel.launchKernel();

        i = 0;
        this->compute_dsi_right_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(census_left_img_buffer_image2d));
        this->compute_dsi_right_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(census_right_img_buffer_image2d));
        this->compute_dsi_right_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(this->dsi_right_buffer));
        this->compute_dsi_right_kernel.launchKernel();

        i = 0;
        this->compute_left_disparity_from_dsi_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(this->dsi_left_buffer));
        this->compute_left_disparity_from_dsi_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(this->left_disparaty_img_buffer_image2d));
        this->compute_left_disparity_from_dsi_kernel.launchKernel();

        i = 0;
        this->compute_right_disparity_from_dsi_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(this->dsi_right_buffer));
        this->compute_right_disparity_from_dsi_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(this->right_disparaty_img_buffer_image2d));
        this->compute_right_disparity_from_dsi_kernel.launchKernel();

        i = 0;
        this->left_right_consistency_check_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(this->left_disparaty_img_buffer_image2d));
        this->left_right_consistency_check_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(this->right_disparaty_img_buffer_image2d));
        this->left_right_consistency_check_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(this->out_img_buffer_image2d));
        this->left_right_consistency_check_kernel.launchKernel();
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        error_msg += "  Code: ";
        error_msg += std::to_string(error.err());
        throw std::runtime_error(error_msg);
    }
}

void SADDisparityEstimator::getResults(cv::Mat& output_left,cv::Mat& output_right)
{
    cl::size_t<3> origin;
    origin[0] = 0;
    origin[1] = 0;
    origin[2] = 0;

    cl::size_t<3> region;
    region[0] = this->image_width;
    region[1] = this->image_height;
    region[2] = 1;

    this->opencl_manager.getCommandQueue().enqueueReadImage(this->out_img_buffer_image2d, CL_TRUE, origin, region, 0, 0, output_left.data);
    this->opencl_manager.getCommandQueue().enqueueReadImage(this->right_disparaty_img_buffer_image2d, CL_TRUE, origin, region, 0, 0, output_right.data);
}