/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// The sampler we will be using for accessing the image buffers
const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP | CLK_FILTER_NEAREST;




__kernel void computeDSIRight(read_only  image2d_t left_img,
                           read_only  image2d_t right_img, 
                           __global uchar* result)
{

    const int2 gid = (int2)(get_global_id(0), get_global_id(1));

    // Make sure we are still in the range of the image
    if ((gid.x >= WIDTH) || (gid.y >= HEIGHT))
    {
        return;
    }

    int disp;
    int cost = 0;
    const int offset = (gid.y * WIDTH * MAX_DISPARITY) + (gid.x * MAX_DISPARITY);

    // Set the max disperity to check to prevent us from reading memory that isnt valid
    int max_disp_to_check = MAX_DISPARITY;
    if((gid.x + MAX_DISPARITY) >= WIDTH)
    {
       max_disp_to_check = WIDTH - gid.x;
    }

    uint4 l;
    uint4 r = read_imageui(right_img, sampler, (int2)(gid.x, gid.y));

    #pragma unroll
    for (disp = 0; disp < max_disp_to_check; disp++)
    {    
        l = read_imageui(left_img, sampler, (int2)(gid.x + disp, gid.y));
        cost = popcount(l.x^r.x);
        cost += popcount(l.y^r.y);

        result[offset + disp] = cost;
    }

    return;
}



__kernel void computeDSILeft(read_only  image2d_t left_img,
                           read_only  image2d_t right_img, 
                           __global uchar* result)
{

    const int2 gid = (int2)(get_global_id(0), get_global_id(1));

    // Make sure we are still in the range of the image
    if ((gid.x >= WIDTH) || (gid.y >= HEIGHT))
    {
        return;
    }

    int disp;
    int cost = 0;

    const int offset = (gid.y * WIDTH * MAX_DISPARITY) + (gid.x * MAX_DISPARITY);

    // Set the max disperity to check to prevent us from reading memory that isnt valid
    int max_disp_to_check = MAX_DISPARITY;
    if((gid.x - MAX_DISPARITY) < 0)
    {
      max_disp_to_check = gid.x;
    }

    uint4 r;
    uint4 l = read_imageui(left_img, sampler, (int2)(gid.x, gid.y));

    #pragma unroll
    for (disp = 0; disp < max_disp_to_check; disp++)
    {    
        r = read_imageui(right_img, sampler, (int2)(gid.x - disp, gid.y));
        cost = popcount(l.x^r.x);
        cost += popcount(l.y^r.y);

        result[offset + disp] = cost;
    }

    return;
}

