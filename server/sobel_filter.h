/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#ifndef SOBEL_FILTER_H
#define SOBEL_FILTER_H

// Project Includes
#include "opencl_kernel.h"
#include "opencl_manager.h"

// OpenCL Includes
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>

// OpenCV Includes
#include <opencv2/core/core.hpp>

class SobelFilter
{
public:

	/** Constructor
	 *  Sets everything up for computing this operation (opencl kernel/buffers/ext)
	 *
	 *  @param opencl_manager_in The manager that is running the opencl show
	 *  @param image_width_in The width of the input image for this operation
	 *  @param image_height_in The height of the input image for this operation
	 *  @throws std::runtime_error if there is an constructing this object (aka initting issue)
	 */
	SobelFilter(OpenCLManager& opencl_manager_in, uint32_t image_width_in, uint32_t image_height_in);

	/** Destructor
	 */
	~SobelFilter();

	/** Compute the operation. Note: this is non-blocking
	 *
	 *  @param input Input opencl buffer
	 *  @throws std::runtime_error if there is an error running the kernel
	 */
	void compute(cl::Image2D& input);
    void compute(cv::Mat& input);

	/** Get the results of the operation
	 *
	 *  @param output The cv::Mat to fill with the data.  This cv::Mat must already be allocated.
	 *  @throws std::runtime_error if there is an error getting the data
	 */
	void getResults(cv::Mat& output);

	/** The a reference to the output buffer so we can move pipeline GPU operations
	 *
	 *  @returns Reference to the output buffer
	 */
	cl::Image2D& getOutputBuffer();

private:

	/** Constants
	 */
	static constexpr uint32_t LOCAL_WORK_GROUP_X = 16;
	static constexpr uint32_t LOCAL_WORK_GROUP_Y = 16;

	/** Init the opencl kernel
	 *
	 *  @throws std::runtime_error if there is an error initting the kernel
	 */
	void initKernel();

	/** Reference to the opencl manager that is running the whole opencl show
	 */
	OpenCLManager& opencl_manager;

	/** The opencl kernel that will do all the processing
	 */
	OpenCLKernel sobel_kernel;

	/** The height and width of the image
	 */
	uint32_t image_width{0};
	uint32_t image_height{0};

	/** The memory size of the image and the buffers
	 */
	uint32_t memory_size{0};

	/** The parameters we will need to use for transferring the images buffers to and from the GPU
	 */
	cl::size_t<3> image_2d_transfer_origin;
	cl::size_t<3> image_2d_transfer_region;

	/** The buffers for this kernel
	 */
    cl::Image2D in_img_buffer;
	cl::Image2D out_img_buffer;
};

#endif // SOBEL_FILTER_H
