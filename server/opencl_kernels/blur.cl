/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// The sampler we will be using for accessing the image buffers
const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP | CLK_FILTER_NEAREST;

__kernel void boxFilterBlurHorizontalPass(read_only image2d_t img, 
                                                write_only image2d_t result)
{
    const int2 gid = (int2)(get_global_id(0), get_global_id(1));

    // Skip all the ones that are not on valid rows
    if(gid.y >= HEIGHT)
    {
        return;
    }

    // Compute the offset used to select which row we are on
    const uchar buffer_size = FILTER_RADIUS*2 + 1;

    int sum = 0;
    int i = 0;
    int pos_x = 0;
    uchar buffer[FILTER_RADIUS*2 + 1];
    uchar buffer_index = 0;


    // Bootstrap the sum
    for(i = -FILTER_RADIUS; i <= FILTER_RADIUS; i++)
    {
        buffer[buffer_index] = read_imagei(img, sampler, (int2)(abs(i), gid.y)).x;
        sum += (int)(buffer[buffer_index]);
        buffer_index = (buffer_index + 1) % buffer_size;
    }

    // The sum holds the first result so save it
    write_imagei(result, (int2)(0, gid.y), (int)((float)sum * NORM_FACTOR));

    // Start at the second result
    for(i = 1; i < WIDTH;i++)
    {
        // Subtract the old position
        sum -= (int)(buffer[buffer_index]);

        // Add the new position
        pos_x = abs(i + FILTER_RADIUS);
        if(pos_x >= WIDTH)
        {
            pos_x = WIDTH + WIDTH - pos_x - 2;
        }

        buffer[buffer_index] = read_imagei(img, sampler, (int2)(pos_x, gid.y)).x;
        sum += (int)(buffer[buffer_index]);
        buffer_index = (buffer_index + 1) % buffer_size;

        // Set the value
        write_imagei(result, (int2)(i, gid.y), (int)((float)sum * NORM_FACTOR));
    }
}

__kernel void boxFilterBlurVerticalPass(read_only image2d_t img, 
                                                write_only image2d_t result)
{
    const int2 gid = (int2)(get_global_id(0), get_global_id(1));

    // Skip all the ones that are not on valid rows
    if(gid.x >= WIDTH)
    {
        return;
    }

    // Compute the offset used to select which row we are on
    const uchar buffer_size = FILTER_RADIUS*2 + 1;

    int sum = 0;
    int i = 0;
    int pos_y = 0;
    uchar buffer[FILTER_RADIUS*2 + 1];
    uchar buffer_index = 0;

    // Bootstrap the sum
    for(i = -FILTER_RADIUS; i <= FILTER_RADIUS; i++)
    {
        buffer[buffer_index] = read_imagei(img, sampler, (int2)(gid.x, abs(i))).x;
        sum += (int)(buffer[buffer_index]);
        buffer_index = (buffer_index + 1) % buffer_size;
    }

    // The sum holds the first result so save it
    write_imagei(result, (int2)(gid.x, 0), (int)((float)sum * NORM_FACTOR));

    // Start at the second result
    for(i = 1; i < HEIGHT;i++)
    {
        // Subtract the old position
        sum -= (int)(buffer[buffer_index]);

        // Add the new position
        pos_y = abs(i + FILTER_RADIUS);
        if(pos_y >= HEIGHT)
        {
            pos_y = HEIGHT + HEIGHT - pos_y - 2;
        }

        buffer[buffer_index] = read_imagei(img, sampler, (int2)(gid.x, pos_y)).x;
        sum += (int)(buffer[buffer_index]);
        buffer_index = (buffer_index + 1) % buffer_size;

        // Set the value
        write_imagei(result, (int2)(gid.x ,i), (int)((float)sum * NORM_FACTOR));
    }
}
