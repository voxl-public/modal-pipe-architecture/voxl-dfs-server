/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *	this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *	this list of conditions and the following disclaimer in the documentation
 *	and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *	may be used to endorse or promote products derived from this software
 *	without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *	ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef MODALAI_CONSTANTS_H
#define MODALAI_CONSTANTS_H

// Modal AI includes
#include <modal_pipe_server.h>

/** The name of the processes so we can look it up for the process ID
 */
#define PROCESS_NAME		"voxl-dfs-server"

/** The dimensions of the raw image
 */
#define RAW_IMAGE_WIDTH (640)
#define RAW_IMAGE_HEIGHT (480)

/** The The rescale Factor used for QVGA operations
 */
#define QVGA_RESCALE_FACTOR (1)

/** The name of the calibration files
 */
#define INTRINSIC_CALIB_FILEPATH "/data/modalai/opencv_stereo_intrinsics.yml"
#define EXTRINSIC_CALIB_FILEPATH "/data/modalai/opencv_stereo_extrinsics.yml"

/** The location to save the images to
 */
#define IMAGE_SAVE_DIR	"/data/dfs/"


/****************************************************************************************************
 ** Server Pipe Constants
 ****************************************************************************************************/
#define DISPARITY_PIPE_CH						0
#define DISPARITY_PIPE_NAME						"dfs_disparity"
#define DISPARITY_PIPE_LOCATION					(MODAL_PIPE_DEFAULT_BASE_DIR DISPARITY_PIPE_NAME "/")

#define DISPARITY_SCALED_PIPE_CH				1
#define DISPARITY_SCALED_PIPE_NAME				"dfs_disparity_scaled"
#define DISPARITY_SCALED_PIPE_LOCATION			(MODAL_PIPE_DEFAULT_BASE_DIR DISPARITY_SCALED_PIPE_NAME "/")

#define POINT_CLOUD_PIPE_CH						2
#define POINT_CLOUD_PIPE_NAME					"dfs_point_cloud"
#define POINT_CLOUD_PIPE_LOCATION				(MODAL_PIPE_DEFAULT_BASE_DIR POINT_CLOUD_PIPE_NAME "/")


/****************************************************************************************************
 ** Client Pipe Constants
 ****************************************************************************************************/
#define CAMERA_INPUT_NAME "/run/mpa/stereo/"

#endif // MODALAI_CONSTANTS_H