/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// The sampler we will be using for accessing the image buffers
const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP | CLK_FILTER_NEAREST;

// Constants
#define CENSUS_WINDOW (16)
#define CENSUS_WINDOW_RADIUS (7) 

__kernel void sparseCensusTransform(read_only  image2d_t img,
                              write_only image2d_t result)
{

    const int2 gid = (int2)(get_global_id(0), get_global_id(1));


    // Make sure we are still in the range of the image
    if (((gid.x - CENSUS_WINDOW_RADIUS) < 0) || ((gid.x + CENSUS_WINDOW_RADIUS) >= WIDTH))
    {
        return;
    }

    if (((gid.y - CENSUS_WINDOW_RADIUS) < 0) || ((gid.y + CENSUS_WINDOW_RADIUS) >= HEIGHT))
    {
        return;
    }

    int x;
    int y;
    const int current_pix = read_imageui(img, sampler, gid).x;
    unsigned int census_value[2];
    census_value[0] = 0;
    census_value[1] = 0;
    int counter = 0;
    int index = 0;

    #pragma unroll
    for (x = -CENSUS_WINDOW_RADIUS; x <= CENSUS_WINDOW_RADIUS; x+=2)
    {
	    #pragma unroll
        for (y = -CENSUS_WINDOW_RADIUS; y <= CENSUS_WINDOW_RADIUS; y+=2)
        {
        	int p = read_imageui(img, sampler, (int2)(gid.x + x, gid.y + y)).x;

        	census_value[index] = census_value[index] << 1;
        	if((current_pix-p) > 0)
        	{
        		census_value[index] |= 1;
        	}
            else
            {
                census_value[index] &= (~1);
            }

        	if(counter == 32)
        	{
        		index++;
        	}
        	else
        	{
	        	counter++;
        	}
        }
    }

    write_imageui(result, gid, (uint4)(census_value[0],census_value[1],0,0));
}
