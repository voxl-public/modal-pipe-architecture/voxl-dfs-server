
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "undistort.h"


// AVG 1.4ms min 1.20 ms for vga image on VOXL1 fastest core
int mcv_undistort_image(const uint8_t* input, uint8_t* output, undistort_map_t* map)
{
	// shortcut variables to make code cleaner
	int  height = map->h;
	int   width = map->w;
	int n_pix = width*height;
	bilinear_lookup_t* L = map->L;

	// go through every pixel in input image
	for(int pix=0; pix<n_pix; pix++){

		// check for invalid (blank) pixels
		if(L[pix].I[0]<0){
			output[pix] = 0;
			continue;
		}

		// get indices from index lookup I
		uint16_t x1 = L[pix].I[0];
		uint16_t y1 = L[pix].I[1];

		// don't worry about all the index algebra, the compiler optimizes this
		uint16_t p0 = input[width*y1 + x1];
		uint16_t p1 = input[width*y1 + x1 + 1];
		uint16_t p2 = input[width*(y1+1) + x1];
		uint16_t p3 = input[width*(y1+1) + x1 + 1];

		// multiply add each pixel with weighting
		output[pix] = (	p0*L[pix].F[0] +
						p1*L[pix].F[1] +
						p2*L[pix].F[2] +
						p3*L[pix].F[3])/256;

	}

	return 0;
}

int mcv_init_undistort_map_from_cvmat(int w, int h, undistort_map_t* map, float* cvmat)
{
	map->h = h;
	map->w = w;

	// allocate new map
	// TODO some sanity and error checking here
	map->L = (bilinear_lookup_t*)malloc(w*h*sizeof(bilinear_lookup_t));
	if(map->L==NULL){
		perror("failed to allocate memory for lookup table");
		return -1;
	}
	bilinear_lookup_t* L = map->L;

	for(int v=0; v<h; ++v){
		for(int u=0; u<w; ++u){


			// convert the distorted world coordinate to pixel coordinate
			// in the original distorted image
			float xd = cvmat[2*((v*w)+u)    ];
			float yd = cvmat[2*((v*w)+u) + 1];

			// find 4 corners around that subpixel
			int x1 = floor(xd);
			int x2 = x1+1;
			int y1 = floor(yd);
			int y2 = y1+1;

			// flag a pixel invalid
			int pix = w*v + u;
			if((x1 < 0 || x2 > (w-1)) || (y1 < 0 || y2 > (h-1))){
				L[pix].I[0] = -1;
				L[pix].I[1] = -1;
				continue;
			}

			// populate lookup table with top left corner pixel
			L[pix].I[0] = x1;
			L[pix].I[1] = y1;

			// integer weightings for 4 pixels. Due to truncation, these 4 ints
			// should sum to no more than 255
			L[pix].F[0] = (y2-yd)*(x2-xd)*256;
			L[pix].F[1] = (y2-yd)*(xd-x1)*256;
			L[pix].F[2] = (yd-y1)*(x2-xd)*256;
			L[pix].F[3] = (yd-y1)*(xd-x1)*256;
		}
	}


	return 0;
}


