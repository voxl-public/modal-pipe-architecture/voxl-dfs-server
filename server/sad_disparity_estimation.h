/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef SAD_DISPARITY_ESTIMATION
#define SAD_DISPARITY_ESTIMATION

// Project Includes
#include "opencl_kernel.h"
#include "opencl_manager.h"

// OpenCL Includes
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>

// OpenCV Includes
#include <opencv2/core/core.hpp>


class SADDisparityEstimator
{
public:

	/** Constructor
	 *  Sets everything up for computing this operation (opencl kernel/buffers/ext)
	 *
	 *  @param opencl_manager_in The manager that is running the opencl show
	 *  @param image_width_in The width of the input image for this operation
	 *  @param image_height_in The height of the input image for this operation
	 *  @param max_disparity_in The max disparity allowed. smaller values = faster execution
	 *  @throws std::runtime_error if there is an constructing this object (aka initting issue)
	 */
	SADDisparityEstimator(OpenCLManager& opencl_manager_in, int image_width_in, int image_height_in, int max_disparity_in);

	/** Destructor
	 */
	~SADDisparityEstimator();

	/** Compute the operation. Note: this is non-blocking
	 *
	 *  @param left_image Input left opencl image
	 *  @param right_image Input right opencl image
	 *  @throws std::runtime_error if there is an error running the kernel
	 */
	void compute(cl::Image2D &left_image, cl::Image2D& right_image);

	/** Get the results of the operation
	 *
	 *  @param output The cv::Mat to fill with the data.  This cv::Mat must already be allocated.
	 *  @throws std::runtime_error if there is an error getting the data
	 */
	void getResults(cv::Mat& output_left, cv::Mat& output_right);

private:

	/** Constants
	 */
	static constexpr uint32_t LOCAL_WORK_GROUP_X = 16;
	static constexpr uint32_t LOCAL_WORK_GROUP_Y = 16;
	static constexpr uint32_t INVALID_DISPARITY_VALUE = 0;

	/** Init the Disparity Kernel
	 *
	 *  @throws std::runtime_error if there was an issue initting the kernel
	 */
	void initDisparityKernel();

	/** Init the Census Kernel
	 *
	 *  @throws std::runtime_error if there was an issue initting the kernel
	 */
	void initCensusKernel();

	/** Init the Compute DSI Kernel
	 *
	 *  @throws std::runtime_error if there was an issue initting the kernel
	 */
	void initComputeDSIKernel();

	/** Init the Compute Disparity From DSI Kernel
	 *
	 *  @throws std::runtime_error if there was an issue initting the kernel
	 */
	void initComputeDisparityFromDSIKernel();

	/** Init the Left Right Consistency Check Kernel
	 *
	 *  @throws std::runtime_error if there was an issue initting the kernel
	 */
	void initLeftRightConsistencyCheckKernel();

	/** The opencl manager that is running the whole opencl show
	*/
	OpenCLManager& opencl_manager;

	/** The different kernels we will be using
	 */
	OpenCLKernel disparity_kernel;
	OpenCLKernel census_kernel_l;
	OpenCLKernel census_kernel_r;
	OpenCLKernel compute_dsi_left_kernel;
	OpenCLKernel compute_dsi_right_kernel;
	OpenCLKernel compute_left_disparity_from_dsi_kernel;
	OpenCLKernel compute_right_disparity_from_dsi_kernel;
	OpenCLKernel left_right_consistency_check_kernel;

	/** The height and width of the image
	 */
	int image_width{0};
	int image_height{0};


	int max_disparity{0};
	int sad_window_size{0};

	/** The image buffers for input and output
	 */
	cl::Image2D in_left_img_buffer_image2d;
	cl::Image2D in_right_img_buffer_image2d;
	cl::Image2D out_img_buffer_image2d;
	cl::Image2D census_left_img_buffer_image2d;
	cl::Image2D census_right_img_buffer_image2d;
	cl::Image2D left_disparaty_img_buffer_image2d;
	cl::Image2D right_disparaty_img_buffer_image2d;

	/** Buffers for the dsi image since the adreno 530 does not support cl::Image3D.
	 */
	cl::Buffer dsi_left_buffer;
	cl::Buffer dsi_right_buffer;
};


#endif // SAD_DISPARITY_ESTIMATION