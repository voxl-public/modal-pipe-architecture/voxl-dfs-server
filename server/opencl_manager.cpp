/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// Project Includes
#include "opencl_manager.h"

OpenCLManager::OpenCLManager()
{
    try
    {
        // Get the platforms
        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);
        if (platforms.size() == 0)
        {
            std::string error_msg = "No OpenCL platforms found.";
            throw std::runtime_error(error_msg);

        }

        // Make sure there is only 1 platform. On VOXL there should only be 1 platform
        if (platforms.size() != 1)
        {
            std::string error_msg = "More than 1 platform found, cant handle this case.";
            throw std::runtime_error(error_msg);
        }

        // Create the OpenCL context
        cl_context_properties props[] =
        {
            CL_CONTEXT_PLATFORM,
            (cl_context_properties)(platforms[0])(),
            0
        };
        this->cl_context = cl::Context(CL_DEVICE_TYPE_GPU, props);

        // Get the devices
        this->opencl_devices = cl_context.getInfo<CL_CONTEXT_DEVICES>();

        // create the command queue
        this->command_queue = cl::CommandQueue(cl_context, this->opencl_devices[0]);

    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}

OpenCLManager::~OpenCLManager()
{

}

cl::Context& OpenCLManager::getContext()
{
    return this->cl_context;
}

cl::CommandQueue& OpenCLManager::getCommandQueue()
{
    return this->command_queue;
}

std::vector<cl::Device>& OpenCLManager::getDevices()
{
    return this->opencl_devices;
}

void OpenCLManager::finishQueue()
{
    this->command_queue.finish();
}

cl::Image2D OpenCLManager::convertMatToClImage2D(cv::Mat input)
{
    // Make the image read only!  Also copy the data over to the GPU
    return cl::Image2D (this->getContext(), CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, cl::ImageFormat(CL_R, CL_UNSIGNED_INT8), input.cols, input.rows, 0, input.data, nullptr);
}
