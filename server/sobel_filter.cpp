/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// Project Includes
#include "sobel_filter.h"
#include "config_file.h"

// C/C++ Includes
#include <string>
#include <sstream>
#include <iostream>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// The Kernel variables we will be using to load the opencl kernel code
extern "C" const char sobel_filter_kernel_code[];
extern "C" const size_t sobel_filter_kernel_code_len;

SobelFilter::SobelFilter(OpenCLManager& opencl_manager_in, uint32_t image_width_in, uint32_t image_height_in):
    opencl_manager(opencl_manager_in),
    sobel_kernel(opencl_manager_in, "sobelFilter"),
    image_width(image_width_in),
    image_height(image_height_in)
{

    /** Create the 2 image buffers that we need.  We are assuming the images are 8 bit and thus the total
     *  for each image is just Width x Height
     */
    this->memory_size = static_cast<int>(this->image_width) * static_cast<int>(this->image_height);
    this->in_img_buffer = cl::Image2D (this->opencl_manager.getContext(), CL_MEM_READ_ONLY, cl::ImageFormat(CL_R, CL_UNSIGNED_INT8), this->image_width, this->image_height, 0, nullptr, nullptr);
    this->out_img_buffer = cl::Image2D (this->opencl_manager.getContext(), CL_MEM_READ_WRITE, cl::ImageFormat(CL_R, CL_UNSIGNED_INT8), this->image_width, this->image_height, 0, nullptr, nullptr);

    // Setup the parameters we will need for transfering data to and from the GPU
    this->image_2d_transfer_origin[0] = 0;
    this->image_2d_transfer_origin[1] = 0;
    this->image_2d_transfer_origin[2] = 0;
    this->image_2d_transfer_region[0] = this->image_width;
    this->image_2d_transfer_region[1] = this->image_height;
    this->image_2d_transfer_region[2] = 1;

    // Setup the kernel
    this->initKernel();
}

SobelFilter::~SobelFilter()
{
}

void SobelFilter::initKernel()
{
    try
    {
        // Create the local and global sizes and set them
        int lx = LOCAL_WORK_GROUP_X;
        int ly = LOCAL_WORK_GROUP_Y;
        int gx = OpenCLKernel::roundUp(this->image_width, lx);
        int gy = OpenCLKernel::roundUp(this->image_height, ly);
        this->sobel_kernel.setWorkGroupsAndOffset(cl::NDRange(lx, ly), cl::NDRange(gx, gy), cl::NDRange(0, 0));

        // Compute the local memory size used for the internal local memory buffer
        int local_memory_size = (lx + 2) * (ly + 2);

        // Create the compile options for this kernel
        std::stringstream options("");
        options << " -DWIDTH="              << this->image_width;
        options << " -DHEIGHT="             << this->image_height;
        options << " -DSIZE="               << this->memory_size;
        options << " -DLOCAL_SIZE="         << local_memory_size;
        options << " -DLOCAL_WORK_SIZE_X="  << lx;
        options << " -DLOCAL_WORK_SIZE_Y="  << ly;

        // Compile the kernel!
        std::string kernel_code(sobel_filter_kernel_code);
        this->sobel_kernel.compileKernel(kernel_code, options.str());
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}

void SobelFilter::compute(cv::Mat& input)
{
    try
    {
        cl::size_t<3> origin;
        origin[0] = 0;
        origin[1] = 0;
        origin[2] = 0;

        cl::size_t<3> region;
        region[0] = this->image_width;
        region[1] = this->image_height;
        region[2] = 1;

        this->opencl_manager.getCommandQueue().enqueueWriteImage(in_img_buffer, CL_FALSE, origin, region, 0, 0, input.data);

        compute(in_img_buffer);

    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}

void SobelFilter::compute(cl::Image2D& input)
{
    try
    {
        // Fill in the kernel arguments
        int i = 0;
        this->sobel_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(input));
        this->sobel_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(this->out_img_buffer));
        this->sobel_kernel.getKernel().setArg(i++, sizeof(int), &sobel_cutoff);

        // Launch the kernels
        this->sobel_kernel.launchKernel();
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}

void SobelFilter::getResults(cv::Mat& output)
{
    try
    {
        // transfer in a blocking way so this is expensive
        this->opencl_manager.getCommandQueue().enqueueReadImage(this->out_img_buffer, CL_TRUE, this->image_2d_transfer_origin, this->image_2d_transfer_region, 0, 0, output.data);
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}

cl::Image2D& SobelFilter::getOutputBuffer()
{
    return this->out_img_buffer;
}
