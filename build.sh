#!/bin/bash


###############################################################
## Build the build utils
###############################################################
cd build_utils
mkdir -p build 
cd build
cmake -DCMAKE_BUILD_TYPE=Release ../
make
cd ../..

###############################################################
## Build the project
###############################################################
TOOLCHAIN64="/opt/cross_toolchain/aarch64-gnu-4.9.toolchain.cmake"

mkdir -p build64
cd build64
cmake -DCMAKE_TOOLCHAIN_FILE=${TOOLCHAIN64} -DCMAKE_BUILD_TYPE=Release ../
make -j4
cd ../