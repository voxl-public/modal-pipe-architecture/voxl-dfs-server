/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// The sampler we will be using for accessing the image buffers
const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP | CLK_FILTER_NEAREST;


// Constants 
#define LEFT_RIGHT_CONSISTENCY_THRESHOLD (2)

/** Computes the disparity costs from consensus values
 *  
 *  @param left_img The left image from the stereo pair
 *  @param right_img The right image from the stereo pair 
 *  @param result The image to write the result to 
 */
__kernel void censusDisparity(read_only  image2d_t left_img,
                           read_only  image2d_t right_img,
                           write_only image2d_t result)
{

    // Compute the global ID for this thread
    const int2 gid = (int2)(get_global_id(0), get_global_id(1));

    // Make sure we are still within range
    if ((gid.x >= WIDTH) || (gid.y >= HEIGHT))
    {
        return;
    }

    int disp;
    int sad = 0;
    int best_disp = 0;
    int best_disp_value = 255;

    // Set the max disperity to check to prevent us from reading memory that isnt valid
    int max_disp_to_check = MAX_DISPARITY;
    if((gid.x - MAX_DISPARITY) < 0)
    {
        max_disp_to_check = gid.x;
    }

    uint4 r;
    uint4 l = read_imageui(left_img, sampler, (int2)(gid.x, gid.y));

    // Find the best disparity!
    #pragma unroll
    for (disp = 1; disp < max_disp_to_check; disp++)
    {   
        // Compute the cost for the disparity
        r = read_imageui(right_img, sampler, (int2)(gid.x - disp, gid.y));
        sad = popcount(l.x^r.x);
        sad += popcount(l.y^r.y);

        // If the sum of abs diff is smaller than the best then this is the best and so we should say its the best disparity
        if(sad < best_disp_value)
        {
            best_disp_value = sad;
            best_disp = disp;
        }
    }

    // Save the disparity!
    write_imageui(result, gid, best_disp);

    return;
}

/** Computes the best disparity given costs for all the disparity values
 *  
 *  @param dsi The 3D disparity cost volume
 *  @param result The image to write the result to 
 */
__kernel void computeDisparityFromDSI(__global uchar* dsi,
                                write_only image2d_t result)
{
    // Compute the global ID for this thread
    const int2 gid = (int2)(get_global_id(0), get_global_id(1));

    const int filter_rad = 1;
    const int filter_norm_denom = ((filter_rad*2) + 1) * ((filter_rad*2) + 1);

    // Make sure we are still in the range of the image
    if ((gid.x >= (WIDTH - filter_rad)) || (gid.y >= (HEIGHT - filter_rad)))
    {
        return;
    }

    if ((gid.x < filter_rad) || (gid.y < filter_rad))
    {
        return;
    }


    // // Make sure we are still in the range of the image
    // if ((gid.x >= WIDTH) || (gid.y >= HEIGHT))
    // {
    //     return;
    // }

    // compute the offset once so we save compute
    const int offset = (gid.y * WIDTH * MAX_DISPARITY) + (gid.x * MAX_DISPARITY);

    int disp;
    int best_disp = 0;
    int best_disp_cost = 255;
    int cost = 0;

    // Set the max disperity to check to prevent us from reading memory that isnt valid
    int max_disp_to_check = MAX_DISPARITY;
    if((gid.x - MAX_DISPARITY) < 0)
    {
        max_disp_to_check = gid.x;
    }

    int x;
    int y;
    int adjuster;

    // Find the best fitting disparity based on the cost
    #pragma unroll
    for (disp = 0; disp < max_disp_to_check; disp++)
    {   

        cost = 0;
        for(x = -filter_rad;x <= filter_rad; x++)
        {
            for(y = -filter_rad;y <= filter_rad; y++)
            {
                adjuster = (y * WIDTH * MAX_DISPARITY) + (x * MAX_DISPARITY);
                cost += dsi[offset + disp + adjuster];
            }
        }

        // cost = cost / filter_norm_denom;


        //cost = dsi[offset + disp];

        // If the sum of abs diff is smaller than the best then this is the best and so we should say its the best disparity
        if(cost < best_disp_cost)
        {
            best_disp_cost = cost;
            best_disp = disp;
        }
    }

    // Save the disparity!
    write_imageui(result, gid, best_disp);

    return;
}

/** Computes the consistency checker for the left and right input images
 *  
 *  @param disparaty_left The disparity with the left image as the reference
 *  @param disparaty_right The disparity with the right image as the reference
 *  @param result The image to write the result to 
 */
__kernel void leftRightConsistencyCheck(read_only image2d_t disparaty_left,
                                        read_only image2d_t disparaty_right,
                                        write_only image2d_t result)
{

    // Compute the global ID for this thread
    const int2 gid = (int2)(get_global_id(0), get_global_id(1));

    // Make sure we are still in the range of the image
    if ((gid.x >= WIDTH) || (gid.y >= HEIGHT))
    {
        return;
    }

    // Get the left and right disparity value
    char left_disparity_value = read_imageui(disparaty_left, sampler, (int2)(gid.x, gid.y)).x;
    char right_disparity_value = read_imageui(disparaty_right, sampler, (int2)(gid.x - left_disparity_value, gid.y)).x;

    // if the disparity difference is less than this threshold then it is valid otherwise zero out the disparity to show error
    if(abs(right_disparity_value - left_disparity_value) < LEFT_RIGHT_CONSISTENCY_THRESHOLD)
    {
        write_imageui(result, gid, left_disparity_value);
    }
    else
    {
        // Invalid disparity so write invalid value
        write_imageui(result, gid, INVALID_DISPARITY_VALUE);
    }

    return;
}

