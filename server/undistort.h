
#ifndef UNDISTORT_H
#define UNDISTORT_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>


// undistort_map_t points to a big array of these, 1 per pixel
typedef struct bilinear_lookup_t{
	int16_t		I[2]; // backwards map to top left corner of 2x2 square
	uint8_t		F[4]; // 4 coefficients for 4 corners of square
} bilinear_lookup_t;


typedef struct undistort_map_t{
	int w;			// image width
	int h;			// image height
	bilinear_lookup_t* L; // lookup table
} undistort_map_t;


// takes a pointer to the 2-channel CV_32FC2 mat data for an undistortion map
// such as from cv::initUndistortRectifyMap
int mcv_init_undistort_map_from_cvmat(int w, int h, undistort_map_t* map, float* cvmat);

int mcv_undistort_image(const uint8_t* input, uint8_t* output, undistort_map_t* map);




#ifdef __cplusplus
}
#endif

#endif // UNDISTORT_H

